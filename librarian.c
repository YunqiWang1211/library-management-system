#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "book_management.h"
#include "librarian.h"
#include "utility.h"
#include "library.h"

#define CreateNode(p)  p=(Book*)malloc(sizeof(Book));
#define DeleteNode(p)   free((Book*)p); 

//If the login account is an administrator account, the administrator menu page is displayed
void librarianCLI(Book *h) {
	int librarianLoggedIn = 1;
	int option;
	Book *book;
	
	while( librarianLoggedIn ){
		printf("\n Please choose an option:\n 1) Add a book\n 2) Remove a book\n 3) Search for books\n 4) Display all books\n 5) Log out\n Option: ");
		option = optionChoice();
		
		if( option == 1 ) {
			//To satisfy the Book Book parameter requirement in the addBook function, 
			//the Book variable is first used to receive basic information about the Book to be added
			book = add_book_input();
			if(book != NULL) {
				add_book(*book, h);
			}
		}
		else if( option == 2 ) {
			//To satisfy the Book Book parameter requirement in the removeBook function, 
			//the Book variable is first used to receive basic information about the Book to be removed
			book = remove_book_input(h);
			if(book!=NULL){
				remove_book(*book, h);
			}
		}
		else if( option == 3 ) {
			searchCLI(h);
		}
		else if( option == 4 ) {
			display_all(h);
		}
		else if( option == 5 ) {
			librarianLoggedIn = 0;
			printf("\nLogging out...\n");
		}
		else
			printf("\nSorry, the option you entered was invalid, please try again.\n");
	}
	return;
}
//Use the variable book to store information about the book to be added
Book *add_book_input() {
	Book *book;
	book = (Book *)malloc(sizeof(Book));
	int sign = 0;
	char str1[99];
	char str2[99];
	char year[99];
	char copies[99];
	while(sign == 0)
	{
		printf("Enter the title of your book you wish to add: ");
		fgets(str1,99,stdin);
		removeNewLine(str1);
		sign = 1;
		printf("Enter the author of your book you wish to add: ");
		fgets(str2,99,stdin);
		removeNewLine(str2);
		printf("Enter the year that your book you wish to add was released: ");
		fgets(year,99,stdin);
		removeNewLine(year);
		if(atoi(year) == 0){
			printf("\nThe year must be greater than or equal to 0 or a number\n");
			sign = 0;
		}
		printf("Enter the the number of copies of your book you wish to add: ");
		fgets(copies,99,stdin);
		removeNewLine(copies);
		if(atoi(copies) == 0){
			printf("\nThe copies must be greater than or equal to 0 or a number\n");
			sign = 0;
		}
	}
	book->title = (char*)malloc(sizeof(char)*(strlen(str1)+1));
	book->authors = (char*)malloc(sizeof(char)*(strlen(str2)+1));
	strcpy(book->title, str1);
	strcpy(book->authors, str2);
	book->year = atoi(year);
	book->copies = atoi(copies);
	book->next = NULL;
	return book;
}
//Use the variable book to store information about the book to be deleted
Book *remove_book_input(Book *h) {
	int sign = 0;
	char id1[99];
	while(sign == 0)
	{
		printf("Enter the id of your book you wish to remove: ");
		fgets(id1,99,stdin);
		removeNewLine(id1);
		sign = 1;
		if(atoi(id1) == 0){
			printf("\nThe year must be greater than or equal to 0 or a number\n");
			sign = 0;
		}
	}
	Book *book;
	book = h->next;
	while(1)
	{
		if(book->id == atoi(id1))
		{
			break;
		}
		book = book->next;
	}
	return book;
}

