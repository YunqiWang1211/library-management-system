#ifndef USER_GUARD__H 
#define USER_GUARD__H

void userCLI(user *hu, Book *hb, user *name, loan *hl);
int loanbooks(loan *hl, user *name, Book *hb);
int returnbook(user *name, loan *hl, Book *hb);
#endif
