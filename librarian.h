#ifndef LIBRARIAN_GUARD__H 
#define LIBRARIAN_GUARD_H

void librarianCLI(Book *h);
Book *add_book_input();
Book *remove_book_input(Book *h);
#endif
