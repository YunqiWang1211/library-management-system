#ifndef LIBRARY_GUARD__H 
#define LIBRARY_GUARD__H

int loadusers(FILE *file, user *h);
int load_loans(FILE *file, loan *h);
int store_loans(FILE *file, loan *h);
int store_users(FILE *file, user *h);
void searchCLI(Book *h);
int reg(user *h);
user *login(user *h);
#endif
