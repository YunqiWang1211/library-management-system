This system is used to manage a simple library, which functions include user registration, login, borrowing books return books, administrator login, management books information. The following is the development log of this system.

SSH URL
git@gitlab.com:YunqiWang1211/library-management-system.git
HTML URL
https://gitlab.com/YunqiWang1211/library-management-system.git
[History picture](https://gitlab.com/YunqiWang1211/library-management-system/-/blob/main/L__GJ_X_RLC_BSIMP_GRPN6.png)

March 23, 2022   Ver 1.0.0
Today this system first realized the initialization library function, this system reads the library data from the book.txt file and stores in the linked list to realize the creation of the library. Secondly, the system also realized the user login and registration, the system through the user. TXT account information acquisition and storage.

March 24, 2022   Ver 1.0.1
I added the functions of librarian to add and remove books to the system, which I temporarily put in library.c for testing purposes and then moved to librarian. I ran into a bug in removing books. My function didn't work when I deleted the first book, but it worked when I deleted other books. Second, I added a new user and userlist structure to user.c, in order to copy the Book structure to use a linked list to store user information. I used the user structure to build a new function loaduser to build a linked list to store user information.

March 27, 2022   Ver 1.1.0
I'm sorry that I haven't done version control for three days. This is mainly because IN the past three days, I have not only been improving my single function, but also obtaining inspiration of linking functions from last assignment. For the convenience of testing, I have integrated all functions into a file and adopted a main function, userCLI, librarianCLI, SearchCLI concatenates all the individual functions. In these three days, I also completed the functions of borrowing and returning books, and fixed the bug that deleting the first book in the removeBook function would destroy the linked list based on the judgment of the head node.

March 29, 2022   Ver 1.1.1
In this update I implemented the ability to find books by title, year, and author. I also found that the feof used in my "loadbook" function would cause the last book of the file to be read twice, when I used if (fgets! =NULL). At the same time, I also implemented the function of storing the book information, user information and library list in the specified file.

April 07, 2022   Ver 1.2.0
In this version control, I modularized the testc.c file, divided it into several files based on the function's different functions, and used makefiles to link different files. Unlike the makefiles in the previous job, I added make All to compile the files. At the same time, I have added annotations for each code to facilitate different people to understand the function of the program. Secondly, I also solved the problem of hard coding files. Now the library information storage files are specified by the user from the command line parameters. The second parameter is the place where the book information is stored, the third parameter is the place where the user information is stored, and the fourth parameter is the place where the book information is stored.
