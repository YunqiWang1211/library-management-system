#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "book_management.h"
#include "utility.h"

#define CreateNode(p)  p=(Book*)malloc(sizeof(Book));
#define DeleteNode(p)   free((Book*)p); 

//Write the data from the book list to book.txt
int store_books(FILE *file, Book *h){
	Book *headb = h;
	while(headb->next != NULL)
	{
		headb=headb->next;
		fprintf(file, "%d\n", headb->id);
		fprintf(file, "%s\n", headb->title);
		fprintf(file, "%s\n", headb->authors);
		fprintf(file, "%d\n", headb->year);
		fprintf(file, "%d\n", headb->copies);
	}
	return 1;
}
//Read the data in book.txt into the book list
int load_books(FILE *file, Book *h){
	Book *p, *last;
	last = h;
	char str1[99];//Store five basic information about books
  	char str2[99];
  	char str3[99];
  	char str4[99];
  	char str5[99];
	while(!feof(file))
	{
		if(fgets(str1, 99, file))
		{
			removeNewLine(str1);
		}
		else{
			break;
		}		
		if(fgets(str2, 99, file))
		{
			removeNewLine(str2);
		}
		else{
			break;
		}	
		if(fgets(str3, 99, file))
		{
			removeNewLine(str3);
		}
		else{
			break;
		}	
		if(fgets(str4, 99, file))
		{
			removeNewLine(str4);
		}
		else{
			break;
		}	
		if(fgets(str5, 99, file))
		{
			removeNewLine(str5);
		}
		else{
			break;
		}
			CreateNode(p);
			p->title = (char*)malloc(sizeof(char)*(strlen(str2)+1));
			p->authors = (char*)malloc(sizeof(char)*(strlen(str3)+1));
			int a = atoi(str1);
      			int b = atoi(str4);
      			int c = atoi(str5);
			p->id = a;
      			p->year = b;
      			p->copies = c;
      			strcpy(p->title, str2);
      			strcpy(p->authors, str3);
			last->next = p;//Insert the node last in the linked list
			last = p;
	}
	last->next = NULL;//The next of the last node points to NULL
	return 0;
}
//When the logon user is an administrator, the administrator is allowed to add books to the last part of the book list
int add_book(Book book, Book *h){
	Book *head, *p;
	head = h;
	//Walk through the linked list to see if the title exists
	while(head->next != NULL){
		head = head->next;
		if(strcmp(head->title, book.title) == 0){
			printf("\nSorry, there is a same book title.\n");
			return 1;
		}
	}
	p = (Book *)malloc(sizeof(Book));
	p->title = (char*)malloc(sizeof(char)*(strlen(book.title)+1));
	p->authors = (char*)malloc(sizeof(char)*(strlen(book.authors)+1));
	strcpy(p->title, book.title);
	strcpy(p->authors, book.authors);
	p->year = book.year;
	p->copies = book.copies;
	p->id = head->id + 1;
	head = h;
	//Find the last node in the list
	while(head->next != NULL){
		head = head->next;
	}
	head->next = p;//Insert the node last in the linked list
	p->next = NULL;
	return 0;
}
//If the login user is an administrator, the administrator can delete a book based on its ID
int remove_book(Book book, Book *h) {
	Book *move = h;
	Book *premove = move;
	while(move->id != book.id && move->next != NULL)
	{
		premove = move;
		move = move->next;
	}
	if(move->id == book.id)
	{
		//Special if the node to be deleted is the first node
		if(move == h)
		{
			h = move->next;
		}
		else
		{
			premove->next = move->next;
		}
		free(move);
		int b = 1;
		Book *head = h->next;
		while(head != NULL)
		{
			head->id = b;
			head = head->next;
			b++;
		}
		return 0;
	}
	else{
		printf("Sorry, you entered id is not exist.");
	}
	return 1;
}
//finds books with a given title.
//returns a BookList structure, where the field "list" is a list of books, or null if no book with the 
//provided title can be found. The length of the list is also recorded in the returned structure, with 0 in case
//list is the NULL pointer.
BookList find_book_by_title (const char *title, Book *h) {
	Book *head;
	head = h->next;
	
	BookList *byTitle;
	byTitle= (BookList *)malloc(sizeof(BookList));
	byTitle->list = (Book *)malloc(sizeof(Book));
	byTitle->length = 0;
	Book *p, *last;
	last = byTitle->list;
	
	while(head != NULL){
		if(strcmp(head->title, title) == 0){
			p = (Book *)malloc(sizeof(Book));
			p->title = (char*)malloc(sizeof(char)*(strlen(head->title)+1));
			p->authors = (char*)malloc(sizeof(char)*(strlen(head->authors)+1));
			p->id = head->id;
			strcpy(p->title, head->title);
			strcpy(p->authors, head->authors);
			p->year = head->year;
			p->id = head->id;
			p->copies = head->copies;
			last->next = p;
			last = p;
			byTitle->length++;
		}
		head = head->next;
	}
	last->next = NULL;
	if(byTitle->length == 0){
		byTitle->list = NULL;
	}
	
	return *byTitle;
}

//finds books with the given authors.
//returns a Booklist structure, where the field "list" is a newly allocated list of books, or null if no book with the 
//provided title can be found. The length of the list is also recorded in the returned structure, with 0 in case
//list is the NULL pointer.
BookList find_book_by_author (const char *author, Book *h) {
	Book *head;
	head = h->next;
	
	BookList *byAuthor;
	byAuthor= (BookList *)malloc(sizeof(BookList));
	byAuthor->list = (Book *)malloc(sizeof(Book));
	byAuthor->length = 0;
	Book *p, *last;
	last = byAuthor->list;
	
	while(head != NULL){
		if(strcmp(head->authors, author) == 0){
			p = (Book *)malloc(sizeof(Book));
			p->title = (char*)malloc(sizeof(char)*(strlen(head->title)+1));
			p->authors = (char*)malloc(sizeof(char)*(strlen(head->authors)+1));
			p->id = head->id;
			strcpy(p->title, head->title);
			strcpy(p->authors, head->authors);
			p->year = head->year;
			p->id = head->id;
			p->copies = head->copies;
			last->next = p;
			last = p;
			byAuthor->length++;
		}
		head = head->next;
	}
	last->next = NULL;
	if(byAuthor->length == 0){
		byAuthor->list = NULL;
	}
	
	return *byAuthor;
}



//finds books published in the given year.
//returns a Booklist structure, where the field "list" is a list of books, or null if no book with the
//provided title can be found. The length of the list is also recorded in the returned structure, with 0 in case
//list is the NULL pointer.
BookList find_book_by_year (unsigned int year, Book *h) {
	Book *head;
	head = h->next;
	
	BookList *byYear;
	byYear= (BookList *)malloc(sizeof(BookList));
	byYear->list = (Book *)malloc(sizeof(Book));
	byYear->length = 0;
	Book *p, *last;
	last = byYear->list;
	
	while(head != NULL){
		if(head->year == year){
			p = (Book *)malloc(sizeof(Book));
			p->title = (char*)malloc(sizeof(char)*(strlen(head->title)+1));
			p->authors = (char*)malloc(sizeof(char)*(strlen(head->authors)+1));
			p->id = head->id;
			strcpy(p->title, head->title);
			strcpy(p->authors, head->authors);
			p->year = head->year;
			p->id = head->id;
			p->copies = head->copies;
			last->next = p;
			last = p;
			byYear->length++;
		}
		head = head->next;
	}
	last->next = NULL;
	if(byYear->length == 0){
		byYear->list = NULL;
	}
	
	return *byYear;
}
