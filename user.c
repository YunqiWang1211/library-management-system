#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "book_management.h"
#include "library.h"
#include "user.h"
#include "utility.h"

#define CreateNode(p)  p=(Book*)malloc(sizeof(Book));
#define DeleteNode(p)   free((Book*)p); 

//If the login account is a common user, the user menu is displayed
void userCLI(user *hu, Book *hb, user *name, loan *hl) {
	int userLoggedIn = 1;
	int option;
	
	while( userLoggedIn ){
		printf("\n Please choose an option:\n 1) Borrow a book\n 2) Return a book\n 3) Search for books\n 4) Display all books\n 5) Log out\n Option: ");
		option = optionChoice();
		
		if( option == 1 ) {
			loanbooks(hl, name, hb);
		}
		else if( option == 2 ) {
			returnbook(name, hl, hb);
		}
		else if( option == 3 ) {
			searchCLI(hb);
		}
		else if( option == 4 ) {
			display_all(hb);
		}
		else if( option == 5 ) {
			userLoggedIn = 0;
			printf("\nLogging out...\n");
		}
		else
			printf("\nSorry, the option you entered was invalid, please try again.\n");
	}
	return;
}

//Allows users to borrow books by the id of the book. If the copy of the book is 0, the user cannot borrow the book if the book has not been returned
int loanbooks(loan *hl, user *name, Book *hb){
	char a[99];
	int id1;
	int sign = 0;
	loan *p;
	printf("Please enter the book id you wish to borrow: ");
	fgets(a,99,stdin);
	removeNewLine(a);
	id1 = atoi(a);
	loan *headl = hl;
	Book *headb = hb->next;
	while(headl->next != NULL)
	{
		//Check to see if there is a record in the library list. If there is, the library is not allowed
		headl = headl->next;
		if(strcmp(headl->userid, name->username) == 0)
		{
			printf("\nSorry, you have borrowed a book, please return it before borrow anther.\n");
			return 0;
		}
	}
	//Check whether booKID exists
	while(headb->next != NULL)
	{
		if(headb->id == id1)
		{
			sign = 1;
			break;
		}
		headb = headb->next;
	}
	if(sign == 0)
	{
		printf("\nSorry, the book id is not exist.\n");
		return 0;
	}
	if(headb->copies == 0)
	{
		printf("\nSorry, the book copies is 0.\n");
		return 0;
	}
	headb->copies = headb->copies-1;
	p=(loan*)malloc(sizeof(loan));
	p->userid = (char*)malloc(sizeof(char)*(strlen(name->username)+1));
	strcpy(p->userid, name->username);
	p->bookid = id1;
	headl = hl;
	while(headl->next != NULL){
		headl = headl->next;
	}
	headl->next = p;
	p->next = NULL;
	return 1;
}
//Allows regular users to return borrowed books
int returnbook(user *name, loan *hl, Book *hb){
	int sign1 = 0;
	printf("\nLoading...\n");
	loan *headl = hl;
	//Detects whether the user has unreturned books
	while(headl->next != NULL)
	{
		headl = headl->next;
		if(strcmp(name->username, headl->userid)==0)
		{
			sign1 = 1;
			break;
		}
	}
	if(sign1 == 0)
	{
		printf("\nSorry, you haven't borrowed the book yet\n");
		return 0;
	}
	Book *headb = hb;
	int sign2 = 0;
	//Check whether the id entered by the user exists
	while(headb->next != NULL)
	{
		headb = headb->next;
		if(headb->id == headl->bookid)
		{
			sign2 = 1;
			break;
		}
	}
	if(sign2 == 0)
	{
		printf("\nSorry, the book id you entered is not exist.\n");
		return 0;
	}
	headb->copies = headb->copies+1;
	//Return the book with the specified ID and delete the user's library record in the library information list
	loan *move = hl;
	loan *premove = move;
	while(move->bookid != headl->bookid && move->next != NULL)
	{
		premove = move;
		move = move->next;
	}
	if(move->bookid == headl->bookid)
	{
		if(move == hl)
		{
			hl = move->next;
		}
		else
		{
			premove->next = move->next;
		}
		free(move);
		return 1;
	}
	return 1;
}
