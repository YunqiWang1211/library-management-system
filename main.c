#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "book_management.h"
#include "library.h"
#include "user.h"
#include "librarian.h"
#include "utility.h"

#define CreateNode(p)  p=(Book*)malloc(sizeof(Book));
#define DeleteNode(p)   free((Book*)p); 

int main( int argc, char *argv[] )
{
	//Initialize all global variables
	BookList *booklist;
	booklist = (BookList *)malloc(sizeof(BookList));
	booklist->list = (Book *)malloc(sizeof(Book));
	
	userlist *userlist1;
	userlist1 = (userlist *)malloc(sizeof(userlist));
	userlist1->list = (user *)malloc(sizeof(user));

	Book *hb;
	CreateNode(hb);

	user *hu;
	hu=(user*)malloc(sizeof(user));
	
	loan *hl;
	hl=(loan*)malloc(sizeof(loan));

	if (argc != 4)
   	{
        // use the error message 
        	printf("Error\nToo few file names\n");
        // exit the application if there is an error
        	return 0;
    	}

	FILE *fp1;
	FILE *fp2;
	FILE *fp3;
	
	//Load the information in the file into three linked lists
	fp1 = fopen(argv[1], "r");
	fp2 = fopen(argv[2], "r");
	fp3 = fopen(argv[3], "r");
	
	if(fp1 == NULL) {
		printf("\nError, books file does not exist.\n");
		exit(0);
	}
	load_books(fp1, hb);
	
	if(fp2 == NULL) {
		printf("\nError, users file does not exist.\n");
		exit(0);
	}
	loadusers(fp2, hu);
	
	if(fp3 == NULL) {
		printf("\nError, loans file does not exist.\n");
		exit(0);
	}
	load_loans(fp3, hl);
	
	fclose(fp1);
	fclose(fp2);
	fclose(fp3);
	
	//Prompts the user to enter options to display the library's initial menu
	int libraryOpen = 1;
	int option;
	while( libraryOpen ){
		printf("\n Please choose an option:\n 1) Register an account\n 2) Login\n 3) Search for books\n 4) Display all books\n 5) Quit\n Option: ");
		option = optionChoice();
		
		if( option == 1 ) {
			reg(hu);
		}
		else if( option == 2 ) {
			user *name;
			name = login(hu);
			if(name == NULL){
				//TODO
			}
			else if(strcmp(name->username, "librarian")==0){
				librarianCLI(hb);
			}
			else if(name){
				userCLI(hu, hb, name, hl);
			}
		}
		else if( option == 3 ) {
			printf("\nLogging search menu...\n");
			searchCLI(hb);
		}
		else if( option == 4 ) {
			display_all(hb);
		}
		else if( option == 5 ) {
			libraryOpen = 0;
			printf("\nThank you for using the library!\nGoodbye!\n");
		}
		else
			printf("\nSorry, the option you entered was invalid, please try again.\n");
	}
	//Store all the data in the linked list in files
	fp1 = fopen(argv[1], "w");
	fp2 = fopen(argv[2], "w");
	fp3 = fopen(argv[3], "w"); 

	store_books(fp1, hb);
	store_users(fp2, hu);
	store_loans(fp3, hl);

	fclose(fp1);
	fclose(fp2);
	fclose(fp3);
	
	free(booklist);
	free(userlist1);
	
	return 0;
}
