#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "book_management.h"
#include "library.h"
#include "utility.h"

#define CreateNode(p)  p=(Book*)malloc(sizeof(Book));
#define DeleteNode(p)   free((Book*)p); 

//Read information from the user.txt file into the linked list of user information
int loadusers(FILE *file, user *h) {
	user *p, *last;
	last = h;
	char str1[99];
  	char str2[99];
	while (!feof(file))
  	{
      		if (!feof(file))
      		{
              	fgets(str1, 99, file);
              	removeNewLine(str1);
      		}
      		else
      		{
          		break;
      		}
      		if (!feof(file))
      		{
          		fgets(str2, 99, file);
          		removeNewLine(str2);
      		}
      		else
      		{
          		break;
      		}
		p=(user*)malloc(sizeof(user));
		p->username = (char*)malloc(sizeof(char)*(strlen(str1)+1));
      		p->password = (char*)malloc(sizeof(char)*(strlen(str2)+1));
		strcpy(p->username, str1);
      		strcpy(p->password, str2);
		last->next=p;
      		last = p;
	}
	last->next=NULL;
	return 0;
}
//Read the contents of loan.txt into the linked list of loan information
int load_loans(FILE *file, loan *h) {
	loan *p, *last;
	last = h;
	char str1[99];
	char str2[99];
	while (!feof(file))
  	{
      		if (!feof(file))
      		{
              		fgets(str1, 99, file);
              		removeNewLine(str1);
      		}
      		else
      		{
          		break;
      		}
      		if (!feof(file))
      		{
          		fgets(str2, 99, file);
          		removeNewLine(str2);
      		}
      		else
      		{
          		break;
      		}
		p=(loan*)malloc(sizeof(loan));
		p->userid = (char*)malloc(sizeof(char)*(strlen(str1)+1));
		strcpy(p->userid, str1);
      		p->bookid = atoi(str2);
		last->next=p;
      		last = p;
	}
	last->next=NULL;
	return 0;
}

//Read the contents of the linked list of loan information into loan.txt
int store_loans(FILE *file, loan *h) {
	loan *headl = h;
	while(headl->next != NULL)
	{
		headl=headl->next;
		fprintf(file, "%s\n", headl->userid);
		fprintf(file, "%d\n", headl->bookid);
	}
	return 1;
}

//Read the contents of the linked list of user information into user.txt
int store_users(FILE *file, user *h) {
	user *headu = h;
	while(headu->next != NULL)
	{
		headu=headu->next;
		fprintf(file, "%s\n", headu->username);
		fprintf(file, "%s\n", headu->password);
	}
	return 1;
}

//When the user selects Find book information, the Find Book menu is displayed
void searchCLI(Book *h) {
	int searching = 1;
	int option;
	char *information;
	information = (char*)malloc(sizeof(char)*1024);
	
	while( searching ){
		printf("\n Please choose an option:\n 1) Find books by title\n 2) Find books by author\n 3) Find books by year\n 4) Return to previews menu\nOption: ");
		option = optionChoice();
		
		if( option == 1 ) {
			printf("Please enter a title: ");
			fgets(information,99,stdin);
			removeNewLine(information);
			display_found(find_book_by_title(information, h));
		}
		else if( option == 2 ) {
			printf("Please enter an author: ");
			fgets(information,99,stdin);
			removeNewLine(information);
			display_found(find_book_by_author(information, h));
		}
		else if( option == 3 ) {
			printf("Please enter a year: ");
			fgets(information,99,stdin);
			removeNewLine(information);
			if(atoi(information) == 0){
				printf("the year must be a number.");
			}
			else {
				display_found(find_book_by_year(atoi(information), h));
			}
		}
		else if( option == 4 ) {
			searching = 0;
			printf("\nReturn to previews menu...\n");
		}
		else
			printf("\nSorry, the option you entered was invalid, please try again.\n");
	}
	return;

}

//Allows new users to register
int reg(user *h) {
	char a[99];
	char b[99];
	
	user *head, *p;
	head = h->next;
	printf("\nPlease enter a username: ");
	fgets(a,99,stdin);
	removeNewLine(a);
	//Check whether the user name exists
	while(head != NULL){
		if(strcmp(head->username, a)==0){
			printf("\nSorry, registration unsuccessful, the username you entered already exists\n");
			return 1;
		}
		head = head->next;
	}
	printf("\nPlease enter a passward: ");
	fgets(b,99,stdin);
	p = (user *)malloc(sizeof(user));
	p->username = (char*)malloc(sizeof(char));
	p->password = (char*)malloc(sizeof(char));
	strcpy(p->username, a);
	strcpy(p->password, b);
	head = h;
	while(head->next != NULL){
		head = head->next;
	}
	head->next = p;
	p->next = NULL;
	
	printf("\nRegistered library account successfully!\n");
	return 0;
}

//Allows users to log in by entering a user name and password
user *login(user *h) {
	char enteredname[30];
	char enteredpass[30];
	user *head;
	head = h->next;
	int sign = 0;
	printf("\nPlease enter a username: ");
	fgets(enteredname,99,stdin);
	removeNewLine(enteredname);
	printf("\nPlease enter a passward: ");
	fgets(enteredpass,99,stdin);
	removeNewLine(enteredpass);
	//Iterate through the linked list of user information to find whether the user entered the correct account password
	while(head != NULL){
		if(strcmp(head->username, enteredname)==0 && strcmp(head->password, enteredpass)==0){
			sign = 1;
			printf("(Logged in as: %s)\n", enteredname);
			break;
		}
		head = head->next;
	}
	if(sign == 0) {
		printf("\nUsername does not exist or wrong password.\n");
		return NULL;
	}
	if(sign == 1){
		return head;
	}
	return NULL;
}

