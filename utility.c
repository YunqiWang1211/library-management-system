#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "book_management.h"
#include "utility.h"

#define CreateNode(p)  p=(Book*)malloc(sizeof(Book));
#define DeleteNode(p)   free((Book*)p); 

////
// Utility functions to handle "safer" string input
//

// DONT ALTER THESE FUNCTIONS

// read in a line of text and convert to an integer option

int optionChoice( void ) {
    int option = -1;
    char line[80];

    // read in the current line as a string
    fgets(line,80,stdin);

    // atoi() converts string to integer, returns 0 if could not convert
    option = (int)atoi(line);

    return option;
}

// remove newline character from the fgets() input

void removeNewLine(char* string) {

    size_t length = strlen(string);

    if((length > 0) && (string[length-1] == '\n')) {
        string[length-1] ='\0';
    }
    return;
}
//Displays information about all library books
void display_all(Book *h) {
	Book *head;
	head = h;
	//Align the displayed information left
	printf("\n%-5s%-30s%-30s%-10s%-10s\n", "ID", "Title", "Authors", "years", "copies");
	while(head->next != NULL)
	{
		head=head->next;
		printf("%-5d%-30s%-30s%-10d%-10d\n", head->id, head->title, head->authors, head->year, head->copies);
	}
}
//Show information about finding books by title, author, and year
void display_found(BookList theBook) {
	Book *head;
	if(theBook.list == NULL){
		printf("\nSorry, no book is found.\n");
	}
	else {
		head = theBook.list->next;
		//Align the displayed information left
		printf("\n%-5s%-30s%-30s%-10s%-10s\n", "ID", "Title", "Authors", "years", "copies");
		while(head != NULL)
		{
			printf("%-5d%-30s%-30s%-10d%-10d\n", head->id, head->title, head->authors, head->year, head->copies);
			head=head->next;
		}
	}
}

